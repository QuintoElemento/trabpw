-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2017 at 03:25 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trabNetflix`
--

-- --------------------------------------------------------

--
-- Table structure for table `generos`
--

CREATE TABLE IF NOT EXISTS `generos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generos`
--

INSERT INTO `generos` (`id`, `nome`, `descricao`) VALUES
(1, 'Comédia', 'Comédia boa pra caramba'),
(2, 'Horror', 'Horror bom pra caramba'),
(3, 'Drama', 'Drama bom pra caramba'),
(4, 'Slice of life', 'Slice of life bom pra caramba'),
(5, 'Suspense', 'Suspense bom pra caramba');

-- --------------------------------------------------------

--
-- Table structure for table `generos_preferidos`
--

CREATE TABLE IF NOT EXISTS `generos_preferidos` (
  `generos_id` int(11) NOT NULL,
  `perfis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generos_preferidos`
--

INSERT INTO `generos_preferidos` (`generos_id`, `perfis_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `perfis`
--

CREATE TABLE IF NOT EXISTS `perfis` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `dataNasc` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfis`
--

INSERT INTO `perfis` (`id`, `idUsuario`, `nome`, `dataNasc`) VALUES
(1, 1, '1º perfil', '2015-07-22');

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

CREATE TABLE IF NOT EXISTS `series` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text,
  `idadeRecomendada` varchar(45) DEFAULT NULL,
  `avaliacao` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `series`
--

INSERT INTO `series` (`id`, `nome`, `descricao`, `idadeRecomendada`, `avaliacao`) VALUES
(1, '1ª série', 'Sobre um cachorrinho e um gatinho que vivem brigando.', '10 anos', 5);

-- --------------------------------------------------------

--
-- Table structure for table `series_has_generos`
--

CREATE TABLE IF NOT EXISTS `series_has_generos` (
  `series_id` int(11) NOT NULL,
  `generos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `series_has_generos`
--

INSERT INTO `series_has_generos` (`series_id`, `generos_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL DEFAULT '2',
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `tipo`, `senha`) VALUES
(1, '1º usuário', 'user1@gmail.com', 1, '3405');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `duracao` time NOT NULL,
  `descricao` text,
  `avaliacao` float DEFAULT NULL,
  `idSerie` int(11) DEFAULT NULL COMMENT 'Se o vídeo pertencer a alguma série, irá possuir idSerie e a temporada a que pertence, e a idadeRecomendada, os gêneros e a avaliação estarão presentes na tabela series. Se o vídeo, por outro lado, for algo independente, não possuirá nem idSerie nem temporada, mas possuirá idadeRecomendada, gêneros e avaliação.',
  `temporada` int(11) DEFAULT NULL,
  `idadeRecomendada` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `nome`, `duracao`, `descricao`, `avaliacao`, `idSerie`, `temporada`, `idadeRecomendada`) VALUES
(1, '1º vídeo', '01:00:00', 'O 1º episódio de uma série sobre um cachorrinho e um gatinho que vivem brigando.', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos_assistidos`
--

CREATE TABLE IF NOT EXISTS `videos_assistidos` (
  `videos_id` int(11) NOT NULL,
  `perfis_id` int(11) NOT NULL,
  `duracaoAssistida` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `videos_has_generos`
--

CREATE TABLE IF NOT EXISTS `videos_has_generos` (
  `videos_id` int(11) NOT NULL,
  `generos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generos_preferidos`
--
ALTER TABLE `generos_preferidos`
  ADD PRIMARY KEY (`generos_id`,`perfis_id`),
  ADD KEY `fk_generos_has_perfis_perfis1_idx` (`perfis_id`),
  ADD KEY `fk_generos_has_perfis_generos1_idx` (`generos_id`);

--
-- Indexes for table `perfis`
--
ALTER TABLE `perfis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_perfis_usuarios_idx` (`idUsuario`);

--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `series_has_generos`
--
ALTER TABLE `series_has_generos`
  ADD PRIMARY KEY (`series_id`,`generos_id`),
  ADD KEY `fk_series_has_generos_generos1_idx` (`generos_id`),
  ADD KEY `fk_series_has_generos_series1_idx` (`series_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_videos_series1_idx` (`idSerie`);

--
-- Indexes for table `videos_assistidos`
--
ALTER TABLE `videos_assistidos`
  ADD PRIMARY KEY (`videos_id`,`perfis_id`),
  ADD KEY `fk_filmes_has_perfis_perfis1_idx` (`perfis_id`),
  ADD KEY `fk_filmes_has_perfis_filmes1_idx` (`videos_id`);

--
-- Indexes for table `videos_has_generos`
--
ALTER TABLE `videos_has_generos`
  ADD PRIMARY KEY (`videos_id`,`generos_id`),
  ADD KEY `fk_filmes_has_generos_generos1_idx` (`generos_id`),
  ADD KEY `fk_filmes_has_generos_filmes1_idx` (`videos_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `generos`
--
ALTER TABLE `generos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `perfis`
--
ALTER TABLE `perfis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `generos_preferidos`
--
ALTER TABLE `generos_preferidos`
  ADD CONSTRAINT `fk_generos_has_perfis_generos1` FOREIGN KEY (`generos_id`) REFERENCES `generos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_generos_has_perfis_perfis1` FOREIGN KEY (`perfis_id`) REFERENCES `perfis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `perfis`
--
ALTER TABLE `perfis`
  ADD CONSTRAINT `fk_perfis_usuarios` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `series_has_generos`
--
ALTER TABLE `series_has_generos`
  ADD CONSTRAINT `fk_series_has_generos_generos1` FOREIGN KEY (`generos_id`) REFERENCES `generos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_series_has_generos_series1` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk_videos_series1` FOREIGN KEY (`idSerie`) REFERENCES `series` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `videos_assistidos`
--
ALTER TABLE `videos_assistidos`
  ADD CONSTRAINT `fk_filmes_has_perfis_filmes1` FOREIGN KEY (`videos_id`) REFERENCES `videos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_filmes_has_perfis_perfis1` FOREIGN KEY (`perfis_id`) REFERENCES `perfis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `videos_has_generos`
--
ALTER TABLE `videos_has_generos`
  ADD CONSTRAINT `fk_filmes_has_generos_filmes1` FOREIGN KEY (`videos_id`) REFERENCES `videos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_filmes_has_generos_generos1` FOREIGN KEY (`generos_id`) REFERENCES `generos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
