<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <?php
        include_once './classes/conexao.class.php';
        include_once './classes/perfil.class.php';
        include_once './classes/usuario.class.php';
        include_once './classes/genero.class.php';
        include_once './classes/serie.class.php';
        include_once './classes/video.class.php';

        $u1 = new usuario();
        $u1->cadastroUsuario('Carlos', 'carlos@gmail.com', '123');
        $u1->listarUsuario();

        echo '<hr>';
        $u2 = new usuario();
        $u2->login(1, 3405);
        $u2->listarUsuario();

        echo '<hr>';
        $p1 = $u2->criaPerfil('Luana', '1990-10-2', [2, 4]);
        $p2 = $u2->criaPerfil('Luan', '1995-10-2', [1, 3]);
        $u2->listarPerfis();
        $u2->apagaPerfil($p1);
        $u2->listarPerfis();

        echo '<hr>';
        $p3 = $u1->criaPerfil('Joanina', '2000-11-4', [1, 4]);
        $p4 = $u1->criaPerfil('João', '2005-11-4', [3, 2]);
        $u1->listarPerfis();

        $v1 = $u2->uploadVideo('2º vídeo', '02:10:00', 'Descrição do 2º vídeo', [1, 3], '15 anos', 3);
        $v1->listarVideo();
        $v1->assistir($p2, '00:30:00');
        echo '<br><br>';

        $s1 = new serie();
        $s1->selecionarSerie(1);
        $s1->listarSerie();
        

        $s2 = new serie();
        $s2->novaSerie($u2, '2ª série', 'Descrição da 2ª série', [1, 4], 2, '5 anos');
        $s2->listarSerie();
        $s2->avaliar(2);        
        $s2->avaliar(4);        
        $s2->avaliar(4);        
        $s2->avaliar(4);        
        $s2->avaliar(5);        
        $s2->apagarSerie($u2);
        
        echo '<hr>';

        $v2 = $u2->uploadVideo('3º vídeo', '1:10:00', 'Descrição do 3º vídeo', [5, 4], '18 anos', 3);
        $v2->avaliar(5);
        $v2->avaliar(2);
        $v2->avaliar(2);
        $v2->avaliar(2);
        //$v3 = $u1->uploadVideo('3º vídeo', '1:10:00', 'Descrição do 3º vídeo', [5, 4], '18 anos', 3);
        //$v2->apagarVideo($u1);
        $v2->apagarVideo($u2);

        $g1 = new genero();
        $g1->selecionarGenero(2);
        $g1->listarGenero();

        $g2 = new genero();
        $g2->novoGenero($u2, 'Estrangeiro');
        $g2->listarGenero();
        $g2->apagarGenero($u2);

        echo '<hr>';
        
        $u2->encerrarConta();
        ?>
    </body>
</html>
