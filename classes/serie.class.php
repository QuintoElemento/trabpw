<?php

class serie {

    protected $id;
    protected $nome;
    protected $desc;
    protected $avaliacao;
    protected $idadeRecomendada;
    protected $genero = [];
    protected $conn;

    function __construct() {
        $this->conn = new conexao();
    }

    function selecionarSerie($id) {
        $result = $this->conn->executa("SELECT * from series where id = " . $id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->id = $id;
                $this->nome = $row['nome'];
                $this->desc = $row['descricao'];
                $this->avaliacao = $row['avaliacao'];
                $this->idadeRecomendada = $row['idadeRecomendada'];

                $result2 = $this->conn->executa("SELECT series_has_generos.generos_id as genID FROM generos JOIN series_has_generos ON generos.id = series_has_generos.generos_id WHERE series_has_generos.series_id = " . $this->id);
                $b = 0;
                while ($row2 = $result2->fetch_assoc()) {
                    $this->genero[$b] = $row2['genID'];
                    $b++;
                }

                echo '*Série selecionada com sucesso*<br>';
            }
        } else {
            echo 'Erro ao selecionar série!' . $result->error;
        }
    }

    function novaSerie($usuario, $nome, $desc, $generos, $avaliacao = 'null', $idadeRecomendada = 'null') {
        if ($usuario->getTipo() == 1) {
            $this->nome = $nome;
            $this->desc = $desc;
            $this->avaliacao = $avaliacao;
            $this->idadeRecomendada = $idadeRecomendada;

            if ($this->conn->executa("INSERT INTO series (nome, descricao, avaliacao, idadeRecomendada) VALUES ('$nome', '$desc', $avaliacao, '$idadeRecomendada')")) {
                $result2 = $this->conn->executa("SELECT id FROM series ORDER BY id DESC LIMIT 1");
                if ($result2->num_rows > 0) {
                    while ($linha = $result2->fetch_assoc()) {
                        $this->id = $linha['id'];
                    }

                    for ($x = 0; $x < count($generos); $x++) {

                        if ($this->conn->executa("INSERT INTO series_has_generos (generos_id, series_id) VALUES ('$generos[$x]', $this->id)")) {
                            $this->genero[$x] = $generos[$x];
                        } else {
                            echo 'Erro ao tentar cadastrar gêneros' . $x;
                        }
                    }
                    echo 'Série cadastrada com sucesso<br>';
                } else {
                    echo '2Erro ao tentar cadastrar série';
                }
            } else {
                echo '1Erro ao tentar cadastrar série';
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function listarSerie() {
        $result = $this->conn->executa('SELECT * FROM series WHERE id = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "ID: " . $row['id'] . "<br>";
                echo "Nome: " . $row['nome'] . "<br>";
                echo "Descrição: " . $row['descricao'] . "<br>";
                echo "Avaliação: " . $row['avaliacao'] . "<br>";
                echo "Idade recomendada: " . $row['idadeRecomendada'] . " anos<br>";
            }
            $result4 = $this->conn->executa('SELECT count(id) AS nEpi FROM videos WHERE idSerie = ' . $this->id);

            if ($result4->num_rows > 0) {
                while ($row = $result4->fetch_assoc()) {
                    echo "Número de episódios: " . $row['nEpi'] . "<br>";
                }
            }

            $result5 = $this->conn->executa('SELECT series_has_generos.generos_id AS idGenero FROM series_has_generos WHERE series_has_generos.series_id = ' . $this->id);
            if ($result5->num_rows > 0) {
                echo '<h4>Gêneros: </h4>';

                while ($row = $result5->fetch_assoc()) {
                    $g = new genero();
                    $g->selecionarGenero($row['idGenero']);
                    $g->listarGenero();
                }
                echo '<hr>';
            }
        } else {
            echo 'Erro ao tentar exibir a série!';
        }
    }

    function apagarSerie($usuario) {
        if ($usuario->getTipo() == 1) {
            if ($this->conn->executa("DELETE FROM series_has_generos WHERE series_id = " . $this->id)) {
                if ($this->conn->executa("DELETE FROM series WHERE id = " . $this->id)) {
                    echo 'Série deletada.';
                } else {
                    echo 'Erro ao tentar apagar a série.' . $result->error;
                }
            } else {
                echo 'Erro ao tentar apagar a série.' . $result->error;
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function avaliar($avaliacao) {
        $result = $this->conn->executa("SELECT avaliacao FROM series WHERE id = " . $this->id . " LIMIT 1");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<br>Avaliação: " . $row['avaliacao'];
                $this->conn->executa("UPDATE series SET avaliacao = " . (($avaliacao + $row['avaliacao']) / 2) . " WHERE id = " . $this->id);

                $result2 = $this->conn->executa("SELECT avaliacao FROM series WHERE id = " . $this->id . " LIMIT 1");
                if ($result2->num_rows > 0) {
                    while ($row2 = $result2->fetch_assoc()) {
                        echo "<br>Nova avaliação: " . $row2['avaliacao'];
                        echo '<br>';
                    }
                }
            }
        } else {
            echo 'Erro ao tentar avaliar.' . $result->error;
        }
    }

}
