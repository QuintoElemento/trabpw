<?php

class video {

    protected $id;
    protected $nome;
    protected $duracao;
    protected $desc;
    protected $avaliacao;
    protected $idSerie;
    protected $temp;
    protected $idadeRecomendada;
    protected $genero = [];
    protected $conn;

    function __construct() {
        $this->conn = new conexao();
    }

    function selecionarVideo($id) {
        $result = $this->conn->executa("SELECT * from videos where id = " . $id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $result3 = $this->conn->executa('SELECT series.id AS idSerie FROM series WHERE  id = ' . $this->idSerie);
                if ($result3->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $s = new serie();
                        $s->selecionarSerie($row['idSerie']);
                        $s->listarSerie();
                    }
                    echo '<hr>';
                }

                $this->id = $id;
                $this->nome = $row['nome'];
                $this->duracao = $row['duracao'];
                $this->desc = $row['descricao'];
                $this->avaliacao = $row['avaliacao'];
                $this->idSerie = $row['idSerie'];
                $this->temp = $row['temp'];
                $this->idadeRecomendada = $row['idadeRecomendada'];

                $result2 = $this->conn->executa("SELECT videos_has_generos.generos_id as genID FROM generos JOIN videos_has_generos ON generos.id = videos_has_generos.generos_id WHERE videos_has_generos.videos_id = " . $this->id);
                $b = 0;
                while ($row2 = $result2->fetch_assoc()) {
                    $this->genero[$b] = $row2['genID'];
                    $b++;
                }

                echo '*Vídeo selecionado com sucesso*<br>';
            }
        } else {
            echo 'Erro ao selecionar vídeo!' . $result->error;
        }
    }

    function novoVideo($usuario, $nome, $duracao, $desc, $generos, $avaliacao = 'null', $idSerie = 'null', $temp = 'null', $idadeRecomendada = 'null') {
        if ($usuario->getTipo() == 1) {
            $this->nome = $nome;
            $this->duracao = $duracao;
            $this->desc = $desc;
            $this->avaliacao = $avaliacao;
            $this->idSerie = $idSerie;
            $this->temp = $temp;
            $this->idadeRecomendada = $idadeRecomendada;

            if ($this->conn->executa("INSERT INTO videos (nome, duracao, descricao, avaliacao, idSerie, temporada, idadeRecomendada) VALUES ('" . $nome . "', '" . $duracao . "', '" . $desc . "', " . $avaliacao . ", " . $idSerie . ", " . $temp . ", '" . $idadeRecomendada . "')")) {
                $result2 = $this->conn->executa("SELECT id FROM videos ORDER BY id DESC LIMIT 1");
                if ($result2->num_rows > 0) {
                    while ($linha = $result2->fetch_assoc()) {
                        $this->id = $linha['id'];
                    }

                    for ($x = 0; $x < count($generos); $x++) {
                        if ($this->conn->executa("INSERT INTO videos_has_generos (generos_id, videos_id) VALUES ('$generos[$x]]', $this->id)")) {
                            $this->genero[$x] = $generos[$x];
                        } else {
                            echo 'Erro ao tentar cadastrar gêneros preferidos ' . $x;
                        }
                    }
                    echo 'Vídeo cadastrado com sucesso<br>';
                } else {
                    echo 'Erro ao tentar cadastrar vídeo';
                }
            } else {
                echo 'Erro ao tentar cadastrar vídeo';
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function listarVideo() {
        $result = $this->conn->executa('SELECT * FROM videos WHERE id = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "ID: " . $row['id'] . "<br>";
                echo "Nome: " . $row['nome'] . "<br>";
                echo "Duração: " . $row['duracao'] . "<br>";
                echo "Descrição: " . $row['descricao'] . "<br>";
                echo "Avaliação: " . $row['avaliacao'] . "<br>";
                echo "ID da série: " . $row['idSerie'] . "<br>";
                echo "Temporada: " . $row['temporada'] . "<br>";
                echo "Idade recomendada: " . $row['idadeRecomendada'] . " anos<br>";
            }

            $result2 = $this->conn->executa('SELECT generos.id AS idGenero FROM generos JOIN videos_has_generos ON generos.id = videos_has_generos.generos_id WHERE videos_has_generos.videos_id = ' . $this->id);
            if ($result2->num_rows > 0) {
                echo '<h4>Gêneros: </h4>';

                while ($row = $result2->fetch_assoc()) {
                    $g = new genero();
                    $g->selecionarGenero($row['idGenero']);
                    $g->listarGenero();
                }
                echo '<hr>';
            }
        } else {
            echo 'Erro ao tentar exibir o vídeo!';
        }
    }

    function assistir($perfil, $duracao) {
        if ($this->conn->executa('SELECT * FROM videos_assistidos where videos_id = ' . $this->id . ' AND perfis_id = ' . $perfil->getId())->num_rows > 0) {
            $this->conn->executa('UPDATE videos_assistidos SET duracaoAssistida = "' . $duracao . '" WHERE videos_id = ' . $this->id . ' AND perfis_id = ' . $perfil->getId());
            echo 'Os dados referentes a uma série já assistida foram atualizados';
        } else {
            $this->conn->executa("INSERT INTO videos_assistidos (videos_id, perfis_id, duracaoAssistida) VALUES (" . $this->id . ", " . $perfil->getId() . ",'" . $duracao . "')");
            echo 'Uma nova série foi assistida';
        }
    }

    function apagarVideo($usuario) {
        if ($usuario->getTipo() == 1) {
            if ($this->conn->executa("DELETE FROM videos_has_generos WHERE videos_id = " . $this->id)) {
                if ($this->conn->executa("DELETE FROM videos WHERE id = " . $this->id)) {
                    echo '<br>Vídeo apagado.';
                } else {
                    echo 'Erro ao tentar apagar o vídeo.' . $result->error;
                }
            } else {
                echo 'Erro ao tentar apagar o vídeo.' . $result->error;
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function avaliar($avaliacao) {
        $result = $this->conn->executa("SELECT avaliacao FROM videos WHERE id = " . $this->id . " LIMIT 1");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<br>Avaliação: " . $row['avaliacao'];
                $this->conn->executa("UPDATE videos SET avaliacao = " . (($avaliacao + $row['avaliacao']) / 2) . " WHERE id = " . $this->id);

                $result2 = $this->conn->executa("SELECT avaliacao FROM videos WHERE id = " . $this->id . " LIMIT 1");
                if ($result2->num_rows > 0) {
                    while ($row2 = $result2->fetch_assoc()) {
                        echo "<br>Nova avaliação: " . $row2['avaliacao'];
                        echo '<br>';
                    }
                }
            }
        } else {
            echo 'Erro ao tentar avaliar.' . $result->error;
        }
    }

}
