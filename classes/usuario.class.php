<?php

class usuario {

    protected $id;
    protected $senha;
    protected $nome;
    protected $email;
    protected $tipo;
    protected $conn;

    function __construct() {
        $this->conn = new conexao();
    }

    function login($id, $senha) {
        $result = $this->conn->executa("SELECT * from usuarios where id = " . $id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                if ($senha == $row['senha']) {
                    $this->nome = $row['nome'];
                    $this->email = $row['email'];
                    $this->tipo = $row['tipo'];
                    $this->senha = $row['senha'];
                    $this->id = $id;
                    echo '<hr>Usuário logado';
                } else {
                    echo '<script>alert("Senha incorreta.");</script>';
                }
            }
        } else {
            echo 'Erro! Nenhum usuário compatível com os dados informados. ' . $result->error;
        }
    }

    function cadastroUsuario($nome, $email, $senha, $tipo = 2) {
        $this->nome = $nome;
        $this->senha = $senha;
        $this->email = $email;
        $this->tipo = $tipo;
        $this->id = $this->armazena($nome, $senha, $email, $tipo);
    }

    function armazena($nome, $senha, $email, $tipo) {
        if ($this->conn->executa("INSERT INTO usuarios (nome, senha, email, tipo) VALUES ('$nome','$senha','$email',$tipo)")) {
            $result = $this->conn->executa("SELECT id FROM usuarios ORDER BY id DESC LIMIT 1");
            if ($result->num_rows > 0) {
                while ($linha = $result->fetch_assoc()) {
                    echo 'Usuário cadastrado com sucesso.';
                    return $linha['id'];
                }
            }
        } else {
            echo 'Erro!' . $result->error;
        }
    }

    function listarUsuario() {
        $result = $this->conn->executa('SELECT * FROM usuarios WHERE id = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<hr>";
                echo "ID: " . $row['id'] . "<br>";
                echo "Nome: " . $row['nome'] . "<br>";
                echo "Senha " . $row['senha'] . "<br>";
                echo "E-mail: " . $row['email'] . "<br>";
                echo "Tipo: " . $row['tipo'] . "<br>";
            }
        }
    }

    function encerrarConta() {
        $result = $this->conn->executa('SELECT perfis.id AS idPerfil FROM perfis WHERE idUsuario = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $p = new perfil();
                $p->loginPerfil($row['idPerfil']);
                $p->apagarPerfil();
                echo '<br>';
            }

            if ($this->conn->executa("DELETE FROM usuarios WHERE id = " . $this->id)) {
                echo 'Conta encerrada';
            } else {
                echo 'Algum erro ocorreu ao tentar encerrar sua conta, tente novamente mais tarde.USUÁRIO';
            }
        }
    }

    function listarPerfis() {
        $result = $this->conn->executa('SELECT perfis.id AS idPerfil FROM perfis WHERE idUsuario = ' . $this->id);
        if ($result->num_rows > 0) {
            echo '<hr><h3>Perfis associados</h3>';

            while ($row = $result->fetch_assoc()) {
                $p = new perfil();
                $p->loginPerfil($row['idPerfil']);
                $p->listarPerfil();
            }
            echo '<hr>';
        }
    }

    function criaPerfil($nome, $dataNasc, $genero) {
        $p = new perfil();
        return $p->novoPerfil($nome, $this->getId(), $dataNasc, $genero);
    }

    function apagaPerfil($p) {
        $p->apagarPerfil();
    }

    function uploadVideo($nome, $duracao, $desc, $genero, $idadeRecomendada = 'null', $avaliacao = 'null', $idSerie = 'null', $temp = 'null') {//Falta testar
        if ($this->tipo == 1) {
            $v = new video();
            $v->novoVideo($this, $nome, $duracao, $desc, $genero, $avaliacao, $idSerie, $temp, $idadeRecomendada);
            return $v;
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function apagarVideo($v) {
        if ($this->tipo == 1) {
            $v = new video();
            $v->selecionarVideo($v);
            $v->apagarVideo();
        }
    }

    function setNome($nome) {
        $this->nome = $nome;
        $result = $this->conn->executa("UPDATE usuarios SET nome = '$nome' WHERE id = " . $this->id);
    }

    function getNome() {
        return $this->nome;
    }

    function setEmail($email) {
        $this->email = $email;
        $result = $this->conn->executa("UPDATE usuarios SET email = '$email' WHERE id = " . $this->id);
    }

    function getEmail() {
        return $this->email;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
        $result = $this->conn->executa("UPDATE usuarios SET tipo = '$tipo' WHERE id = " . $this->id);
    }

    function getTipo() {
        return $this->tipo;
    }

    function getId() {
        return $this->id;
    }

}
