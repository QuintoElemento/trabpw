<?php

class genero {

    protected $id;
    protected $nome;
    protected $desc;
    protected $conn;

    function __construct() {
        $this->conn = new conexao();
    }

    function selecionarGenero($id) {
        $result = $this->conn->executa("SELECT * FROM generos WHERE id = " . $id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->id = $id;
                $this->nome = $row['nome'];
                $this->desc = $row['descricao'];

                echo '*Gênero selecionado com sucesso*<br>';
            }
        } else {
            echo 'Erro ao selecionar gênero!' . $result->error;
        }
    }

    function novoGenero($usuario, $nome, $desc = null) {
        if ($usuario->getTipo() == 1) {
            $this->nome = $nome;
            $this->desc = $desc;

            if ($this->conn->executa("INSERT INTO generos (nome, descricao) VALUES ('$nome', '$desc')")) {
                $result = $this->conn->executa("SELECT id FROM generos ORDER BY id DESC LIMIT 1");
                if ($result->num_rows > 0) {
                    while ($linha = $result->fetch_assoc()) {
                        $this->id = $linha['id'];
                        echo 'Gênero cadastrado com sucesso';
                    }
                } else {
                    echo 'Erro ao tentar cadastrar gênero';
                }

                return $this;
            } else {
                echo 'Erro ao tentar cadastrar gênero';
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

    function listarGenero() {
        $result = $this->conn->executa('SELECT * FROM generos WHERE id = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "ID: " . $row['id'] . "<br>";
                echo "Nome: " . $row['nome'] . "<br>";
                echo "Descrição: " . $row['descricao'] . "<br><br>";
            }
        } else {
            echo 'Erro ao tentar exibir o gênero!';
        }
    }

    function apagarGenero($usuario) {
        if ($usuario->getTipo() == 1) {
            if ($this->conn->executa("DELETE FROM generos_preferidos WHERE generos_id = " . $this->id)) {
                if ($this->conn->executa("DELETE FROM series_has_generos WHERE generos_id = " . $this->id)) {
                    if ($this->conn->executa("DELETE FROM videos_has_generos WHERE generos_id = " . $this->id)) {
                        if ($this->conn->executa("DELETE FROM generos WHERE id = " . $this->id)) {
                            echo 'Gênero encerrado.';
                        } else {
                            echo 'Erro ao tentar encerrar o gênero.' . $result->error;
                        }
                    }
                }
            } else {
                echo 'Erro ao tentar encerrar o gênero.' . $result->error;
            }
        } else {
            echo 'Você não possui permissão para realizar tal atividade!';
        }
    }

}
