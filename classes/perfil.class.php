<?php

class perfil {

    protected $id;
    protected $nome;
    protected $usuario;
    protected $dataNasc;
    protected $conn;
    protected $genero = [];

    function __construct() {
        $this->conn = new conexao();
    }

    function loginPerfil($id) {
        $result = $this->conn->executa("SELECT * from perfis where id = " . $id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->id = $id;
                $this->nome = $row['nome'];
                $this->usuario = $row['idUsuario'];
                $this->dataNasc = $row['dataNasc'];

                $result2 = $this->conn->executa("SELECT generos_preferidos.generos_id as genID FROM generos JOIN generos_preferidos ON generos.id = generos_preferidos.generos_id WHERE generos_preferidos.perfis_id = " . $this->id);
                $b = 0;
                while ($row2 = $result2->fetch_assoc()) {
                    $this->genero[$b] = $row2['genID'];
                    $b++;
                }
                echo '*Perfil selecionado com sucesso*<br>';
            }
        } else {
            echo 'Erro ao selecionar perfil!' . $result->error;
        }
    }

    function novoPerfil($nome, $usuario, $dataNasc, $genero) {
        $this->nome = $nome;
        $this->usuario = $usuario;
        $this->dataNasc = $dataNasc;

        if ($this->conn->executa("INSERT INTO perfis (nome, idUsuario, dataNasc) VALUES ('$nome', $usuario, '$dataNasc')")) {
            $result = $this->conn->executa("SELECT id FROM perfis ORDER BY id DESC LIMIT 1");
            if ($result->num_rows > 0) {
                while ($linha = $result->fetch_assoc()) {
                    $this->id = $linha['id'];
                }

                for ($x = 0; $x < count($genero); $x++) {
                    if ($this->conn->executa("INSERT INTO generos_preferidos (generos_id, perfis_id) VALUES (" . $genero[$x] . ", " . $this->id . ")")) {
                        $this->genero[$x] = $genero[$x];
                    } else {
                        echo 'Erro ao tentar cadastrar gêneros preferidos ' . $x;
                    }
                }
                echo 'Perfil criado com sucesso.';
            } else {
                echo 'Erro ao tentar cadastrar perfil';
            }

            return $this;
        } else {
            echo 'Erro ao tentar cadastrar perfil';
        }
    }

    function listarPerfil() {
        $result = $this->conn->executa('SELECT usuarios.id AS idUser, usuarios.nome AS nomeUser,perfis.nome AS nomePerfil, perfis.id AS idPerfil, perfis.dataNasc AS dataNasc, TIMESTAMPDIFF(YEAR,perfis.dataNasc,CURDATE()) AS idade from usuarios JOIN perfis ON usuarios.id = perfis.idUsuario WHERE perfis.id = ' . $this->id);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "ID da conta associada: " . $row['idUser'] . "<br>";
                echo "Nome da conta associada: " . $row['nomeUser'] . "<br>";
                echo "ID do perfil: " . $row['idPerfil'] . "<br>";
                echo "Nome do perfil: " . $row['nomePerfil'] . "<br>";
                echo "Data de nascimento: " . $row['dataNasc'] . "<br>";
                echo "Idade: " . $row['idade'] . " anos<br>";
            }

            if ($result2 = $this->conn->executa("SELECT generos.id AS genID, generos.nome AS genNome, generos.descricao AS genDesc FROM generos JOIN generos_preferidos ON generos.id = generos_preferidos.generos_id JOIN perfis ON generos_preferidos.perfis_id = perfis.id WHERE perfis.id = " . $this->id)) {
                echo '<h4>Gêneros preferidos </h4>';
                while ($row = $result2->fetch_assoc()) {
                    echo '<b>ID: </b>' . $row['genID'];
                    echo ' - <b>Nome: </b>' . $row['genNome'];
                    echo ' - <b>Descrição: </b>' . $row['genDesc'] . '<br>';
                }
            } else {
                echo 'Erro ao tentar exibir gêneros preferidos.<br>';
            }
            echo '<hr>';
        } else {
            echo 'Erro ao tentar exibir o perfil!';
        }
    }

    function assistir($id, $duracao) {
        $v = new video();
        $v->selecionarVideo($id);
        $v->assistir($this, $duracao);
    }

    function apagarPerfil() {
        if ($this->conn->executa("DELETE FROM generos_preferidos WHERE perfis_id = " . $this->id)) {
            if ($this->conn->executa("DELETE FROM perfis WHERE id = " . $this->id)) {
                echo 'Perfil encerrado.';
            } else {
                echo 'Erro ao tentar encerrar o perfil.';
            }
        } else {
            echo 'Erro ao tentar encerrar o perfil.';
        }
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getDataNasc() {
        return $this->dataNasc;
    }

    function getConn() {
        return $this->conn;
    }

    function getGenero() {
        return $this->genero;
    }

}
