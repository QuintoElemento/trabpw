<?php

class conexao {

    protected $user = 'root';
    protected $host = 'localhost';
    protected $password = '';
    protected $database = 'trabnetflix';
    protected $link;

    function __construct() {
        $this->link = new mysqli($this->host, $this->user, $this->password, $this->database);

        if ($this->link->connect_error) {
            die("Erro na conexão: " . $conn->connect_error);
        }

        return $this->link;
    }

    function executa($query) {
        $result = mysqli_query($this->link, $query);
        return $result;
    }

}
